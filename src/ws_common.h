#ifndef _WS_COMMON_H_
#define _WS_COMMON_H_

#define WS_PROTOCOL_NAME "earlyboot"
#define WS_DEFAULT_PORT 80
#define WS_DEFAULT_HOST "localhost"
#define WS_DEFAULT_PATH "/"

#define WS_MSG_MAX_LEN  128

#endif /* _WS_COMMON_H_ */
