/**
 * 
 *  @file uart.c
 * 
 *  Copyright (C) 2015  Daniel Kesler <kesler.daniel@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

#include "ebctl.h"
#include "ws_common.h"

#include <stdio.h>
#include <stdlib.h>

#include <libwebsockets.h>
#include <string.h>
#include <signal.h>

#define VERSION	"0.01"

static int interrupted;
static char* message;
static struct lws *client_wsi;

struct control_context {
	int  id;
	unsigned noid;
	char *data;
	unsigned len;
	unsigned wait_for_response;
};

static int
callback_ws_client(struct lws *wsi, enum lws_callback_reasons reason,
	      void *user, void *in, size_t len)
{
	struct control_context *ctx = (struct control_context*)user;
	char raw_data[WS_MSG_MAX_LEN+LWS_PRE];
	int msg_len = 0, r;
	
	switch (reason) {

	/* because we are protocols[0] ... */
	case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
		lwsl_err("CLIENT_CONNECTION_ERROR: %s\n",
			 in ? (char *)in : "(null)");
		client_wsi = NULL;
		break;

	case LWS_CALLBACK_CLIENT_ESTABLISHED:
		//lwsl_user("%s: established\n", __func__);
		//lwsl_warn("DATA: %s [%d]", msg->data, msg->len);
		msg_len = snprintf(raw_data+LWS_PRE, WS_MSG_MAX_LEN, "%s", ctx->data);
		r = lws_write(wsi, raw_data + LWS_PRE, msg_len, LWS_WRITE_TEXT);
		
		if(ctx->wait_for_response == 0) {
			client_wsi = NULL;
		}
		
		break;

	case LWS_CALLBACK_CLIENT_RECEIVE:
		printf("%s\n", (const char*)in);
		if(ctx->wait_for_response == 0 || --ctx->wait_for_response == 0) {
			client_wsi = NULL;
			break;
		}
		
		break;

	case LWS_CALLBACK_CLIENT_CLOSED:
		client_wsi = NULL;
		break;

	default:
		break;
	}

	return lws_callback_http_dummy(wsi, reason, user, in, len);
}

static const struct lws_protocols protocols[] = {
	{
		WS_PROTOCOL_NAME,
		callback_ws_client,
		0,
		0,
	},
	{ NULL, NULL, 0, 0 }
};

static void
sigint_handler(int sig)
{
	interrupted = 1;
}


void ebctl_usage(const char* appname)
{
	printf("%s -h <host> -p <port> [-w] [-i <id>] <message>\n", appname);
	printf("   -h <host>  WebSocket host [default: localhost]\n");
	printf("   -p <port>  WebSocket port [default: 9000]\n");
	printf("   -w         Wait for response\n");
	printf("example:\n");
	printf("%s mode:welcome\n", appname);
}

int ebctl_applet(int argc, const char **argv)
{	
	if(argc < 2)
	{
		ebctl_usage(argv[0]);
		return EXIT_FAILURE;
	}
	
	struct lws_client_connect_info i;
	struct lws_context_creation_info info;
	struct lws_context *context;
	const char *p;
	int n = 0, logs = LLL_ERR;
		/* for LLL_ verbosity above NOTICE to be built into lws, lws
		 * must have been configured with -DCMAKE_BUILD_TYPE=DEBUG
		 * instead of =RELEASE */
		/* | LLL_INFO */ /* | LLL_PARSER */ /* | LLL_HEADER */
		/* | LLL_EXT */ /* | LLL_CLIENT */ /* | LLL_LATENCY */
		/* | LLL_DEBUG */;
		
		
	const char *host = WS_DEFAULT_HOST;
	const char *path = WS_DEFAULT_PATH;
	int port = WS_DEFAULT_PORT;
	unsigned wait_for_response = 0;

	signal(SIGINT, sigint_handler);
	if ((p = lws_cmdline_option(argc, argv, "-d")))
		logs = atoi(p);
		
	if ((p = lws_cmdline_option(argc, argv, "-h")))
		host = p;
		
	if ((p = lws_cmdline_option(argc, argv, "-p")))
		port = atoi(p);
		
	if ((p = lws_cmdline_option(argc, argv, "-w")))
		wait_for_response = 1;
		
	if((p = lws_cmdline_option(argc, argv, "--help"))) {
		ebctl_usage(argv[0]);
		return EXIT_FAILURE;
	}

	lws_set_log_level(logs, NULL);

	memset(&info, 0, sizeof info); /* otherwise uninitialized garbage */
	//info.options = LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;
	info.port = CONTEXT_PORT_NO_LISTEN; /* we do not run any server */
	info.protocols = protocols;

	context = lws_create_context(&info);
	if (!context) {
		lwsl_err("lws init failed\n");
		return 1;
	}

	memset(&i, 0, sizeof i); /* otherwise uninitialized garbage */
	i.context = context;
	i.port = port;
	i.address = host;
	i.path = path;
	i.host = i.address;
	i.origin = i.address;
	i.protocol = protocols[0].name;
	i.pwsi = &client_wsi;
	
	struct control_context ctrlctx;
	
	ctrlctx.data = (char*)argv[argc-1];
	ctrlctx.len = strlen(ctrlctx.data);
	ctrlctx.wait_for_response = wait_for_response;
	
	i.userdata = &ctrlctx;

	lws_client_connect_via_info(&i);

	while (n >= 0 && client_wsi && !interrupted)
		n = lws_service(context, 1000);

	lws_context_destroy(context);
	
	return EXIT_SUCCESS;
}
