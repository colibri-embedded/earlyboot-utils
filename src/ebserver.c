#include "ebserver.h"
#include "ws_common.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <libwebsockets.h>
#include <string.h>
#include <string.h>
#include <signal.h>

#define VERSION	"0.01"

/* one of these created for each message */

struct msg {
	void *payload; /* is malloc'd */
	size_t len;
};

struct msg_element {
	struct msg_element *prev;
	
	int id;
	char* payload;
	size_t len;
	
	struct lws *wsi;
};

/* one of these is created for each client connecting to us */

struct per_session_data__minimal {
	struct per_session_data__minimal *pss_list;
	struct lws *wsi;
	int last; /* the last message number we sent */
};

/* one of these is created for each vhost our protocol is used with */

struct per_vhost_data__minimal {
	struct lws_context *context;
	struct lws_vhost *vhost;
	const struct lws_protocols *protocol;

	struct per_session_data__minimal *pss_list; /* linked-list of live pss*/

	char *mode;
	char *state;
	char *progress;
	char *redirect;
	char *suggested;

	struct msg amsg; /* the one pending message... */
	int current; /* the current message number we are caching */
};

static const struct lws_protocol_vhost_options pvo_mime_map = {
	NULL,				/* "next" pvo linked-list */
	NULL,				/* "child" pvo linked-list */
	".map",				/* file suffix to match */
	"application/octet-stream"		/* mimetype to use */
};

static const struct lws_protocol_vhost_options pvo_mime = {
	&pvo_mime_map,				/* "next" pvo linked-list */
	NULL,				/* "child" pvo linked-list */
	".woff2",				/* file suffix to match */
	"font/woff2"		/* mimetype to use */
};

/* destroys the message when everyone has had a copy of it */

static void
__minimal_destroy_message(void *_msg)
{
	struct msg *msg = _msg;

	free(msg->payload);
	msg->payload = NULL;
	msg->len = 0;
}

static int
callback_ws_server(struct lws *wsi, enum lws_callback_reasons reason,
			void *user, void *in, size_t len)
{
	struct per_session_data__minimal *pss =
			(struct per_session_data__minimal *)user;
	struct per_vhost_data__minimal *vhd =
			(struct per_vhost_data__minimal *)
			lws_protocol_vh_priv_get(lws_get_vhost(wsi),
					lws_get_protocol(wsi));
	int m;

	switch (reason) {
	case LWS_CALLBACK_PROTOCOL_INIT:
		vhd = lws_protocol_vh_priv_zalloc(lws_get_vhost(wsi),
				lws_get_protocol(wsi),
				sizeof(struct per_vhost_data__minimal));
		vhd->context = lws_get_context(wsi);
		vhd->protocol = lws_get_protocol(wsi);
		vhd->vhost = lws_get_vhost(wsi);
		vhd->mode = strdup("loading");
		vhd->state = strdup("welcome");
		vhd->progress = strdup("0/0");
		vhd->redirect = strdup("/,0");
		vhd->suggested = strdup("no-action");
		vhd->current = 0;
		break;
		
	case LWS_CALLBACK_PROTOCOL_DESTROY:
		free(vhd->mode);
		free(vhd->state);
		free(vhd->progress);
		free(vhd->suggested);
		break;

	case LWS_CALLBACK_ESTABLISHED:
		/* add ourselves to the list of live pss held in the vhd */
		
		lwsl_debug("New client\n");
		
		lws_ll_fwd_insert(pss, pss_list, vhd->pss_list);
		pss->wsi = wsi;
		pss->last = vhd->current;
		break;

	case LWS_CALLBACK_CLOSED:
	
		lwsl_debug("Close client\n");
	
		/* remove our closing pss from the list of live pss */
		lws_ll_fwd_remove(struct per_session_data__minimal, pss_list,
				  pss, vhd->pss_list);
		break;

	case LWS_CALLBACK_SERVER_WRITEABLE:
		if (!vhd->amsg.payload)
			break;

		if (pss->last == vhd->current)
			break;
			
		//lwsl_warn("  LWS_CALLBACK_SERVER_WRITEABLE: %s (%d)\n", (char*)(vhd->amsg.payload + LWS_PRE),vhd->amsg.len  );

		/* notice we allowed for LWS_PRE in the payload already */
		m = lws_write(wsi, vhd->amsg.payload + LWS_PRE, vhd->amsg.len,
			      LWS_WRITE_TEXT);
		if (m < (int)vhd->amsg.len) {
			lwsl_err("ERROR %d writing to ws\n", m);
			return -1;
		}

		pss->last = vhd->current;
		break;

	case LWS_CALLBACK_RECEIVE:
		if (vhd->amsg.payload)
			__minimal_destroy_message(&vhd->amsg);

		vhd->amsg.len = len;
		/* notice we over-allocate by LWS_PRE */
		vhd->amsg.payload = malloc(LWS_PRE + WS_MSG_MAX_LEN);
		if (!vhd->amsg.payload) {
			lwsl_user("OOM: dropping\n");
			break;
		}
		
		char new_data[WS_MSG_MAX_LEN+LWS_PRE];
		char msg_data[WS_MSG_MAX_LEN];
		
		memcpy(msg_data, in, len);
		msg_data[len] = '\0';
		memcpy(vhd->amsg.payload+LWS_PRE, in, len);
		*(char*)(vhd->amsg.payload + len+LWS_PRE) = '\0';
		
		int send_to_all = true;
		
		char* dots = strchr(msg_data, ':');
		if(dots) {
			char *msg_content = dots+1;
			
			if(len >= 5 && !strncmp(msg_data, "mode:", 5)) {
				if(vhd->mode)
					free(vhd->mode);
				vhd->mode = strdup(msg_content);
			} else if (len >= 5 && !strncmp(msg_data, "sync:", 5)) {
				vhd->amsg.len = snprintf((char *)vhd->amsg.payload+LWS_PRE, WS_MSG_MAX_LEN, "mode:%s\nstate:%s\nprogress:%s\nredirect:%s\nsuggested:%s", vhd->mode, vhd->state, vhd->progress, vhd->redirect, vhd->suggested);
				send_to_all = false;
			} else if (len >= 6 && !strncmp(msg_data, "state:", 6)) {
				if(vhd->state)
					free(vhd->state);
				vhd->state = strdup(msg_content);
			} else if (len >= 6 && !strncmp(msg_data, "redirect:", 9)) {
				if(vhd->redirect)
					free(vhd->redirect);
				vhd->redirect = strdup(msg_content);
			} else if (len >= 9 && !strncmp(msg_data, "progress:", 9)) {
				if(vhd->progress)
					free(vhd->progress);
				vhd->progress = strdup(msg_content);
			} else if (len >= 9 && !strncmp(msg_data, "suggested:", 10)) {
				if(vhd->suggested)
					free(vhd->suggested);
				vhd->suggested = strdup(msg_content);
			}
			
			vhd->current++;
			
			/*
			 * let everybody know we want to write something to them
			 * as soon as they are ready
			 */
			lws_start_foreach_llp(struct per_session_data__minimal **,
						  ppss, vhd->pss_list) {
				if( (send_to_all && (*ppss)->wsi != wsi) 
				  ||(!send_to_all && (*ppss)->wsi == wsi) 
				  ) {
						lws_callback_on_writable((*ppss)->wsi);
				}
			} lws_end_foreach_llp(ppss, pss_list);	
		
		} 
		
		break;

	default:
		break;
	}

	return 0;
}

static struct lws_protocols protocols[] = {
	{ "http", lws_callback_http_dummy, 0, 0 },
	{
		WS_PROTOCOL_NAME,
		callback_ws_server,
		sizeof(struct per_session_data__minimal),
		128,
		0, NULL, 0
	},
	{ NULL, NULL, 0, 0 } /* terminator */
};

static int interrupted;

void sigint_handler(int sig)
{
	interrupted = 1;
}

/*

ebserver -p <port> -d <www-directory>
 
*/


void ebserver_usage(const char* appname)
{
	printf("%s [-p <port>] [-w <www-root>] [-i <index-file>]\n", appname);
	printf("   -p <port>  		WebSocket port [default: 9000]\n");
	printf("   -w <www-root>	WWW root directory [default: /var/www]\n");
	printf("   -i <index-file>	Index filename [default: index.html]\n");
	printf("example:\n");
	printf("%s -p 9001 -w /my/www/path", appname);
}

int ebserver_applet(int argc,const char **argv) {
		
	struct lws_context_creation_info info;
	struct lws_context *context;
	const char *p;
	int n = 0, logs = LLL_USER | LLL_ERR | LLL_WARN | LLL_INFO
			/*| LLL_NOTICE */
			/* for LLL_ verbosity above NOTICE to be built into lws,
			 * lws must have been configured and built with
			 * -DCMAKE_BUILD_TYPE=DEBUG instead of =RELEASE */
			/* | LLL_INFO */ /* | LLL_PARSER */ /* | LLL_HEADER */
			/* | LLL_EXT */ /* | LLL_CLIENT */ /* | LLL_LATENCY */
			/* | LLL_DEBUG */;
	int port = WS_DEFAULT_PORT;
			
	struct lws_http_mount mount = {
		/* .mount_next */		NULL,		/* linked-list "next" */
		/* .mountpoint */		"/",		/* mountpoint URL */
		/* .origin */			"/var/www",  /* serve from dir */
		/* .def */				"index.html",	/* default filename */
		/* .protocol */			NULL,
		/* .cgienv */			NULL,
		/* .extra_mimetypes */	&pvo_mime,
		/* .interpret */		NULL,
		/* .cgi_timeout */		0,
		/* .cache_max_age */	0,
		/* .auth_mask */		0,
		/* .cache_reusable */		0,
		/* .cache_revalidate */		0,
		/* .cache_intermediaries */	0,
		/* .origin_protocol */		LWSMPRO_FILE,	/* files in a dir */
		/* .mountpoint_len */		1,		/* char count */
		/* .basic_auth_login_file */	NULL,
	};

	signal(SIGINT, sigint_handler);

	//~ if ((p = lws_cmdline_option(argc, argv, "-d")))
		//~ logs = atoi(p);
		
	if(p = lws_cmdline_option(argc, argv, "-p") )
		port = atoi(p);
		
	if(p = lws_cmdline_option(argc, argv, "-w") )
		mount.origin = strdup(p);
		
	if(p = lws_cmdline_option(argc, argv, "-i") )
		mount.def = strdup(p);
		
	if((p = lws_cmdline_option(argc, argv, "--help"))) {
		ebserver_usage(argv[0]);
		return EXIT_FAILURE;
	}

	lws_set_log_level(logs, NULL);
	lwsl_info("Earlyboot ws server | visit http://localhost:%d\n", port);

	memset(&info, 0, sizeof info); /* otherwise uninitialized garbage */
	info.port = port;
	info.mounts = &mount;
	info.protocols = protocols;

	context = lws_create_context(&info);
	if (!context) {
		lwsl_err("lws init failed\n");
		return EXIT_FAILURE;
	}

	while (n >= 0 && !interrupted)
		n = lws_service(context, 1000);

	lws_context_destroy(context);

	return EXIT_SUCCESS;
}
